/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package my.bookstore.core.constants;

/**
 * Global class for all BookstoreCore constants. You can add global constants for your extension into this class.
 */
public final class BookstoreCoreConstants extends GeneratedBookstoreCoreConstants
{
	public static final String EXTENSIONNAME = "bookstorecore";

	private BookstoreCoreConstants()
	{
		//empty

		//git tets staging
		//git checkout test
		//testing  GIT add and commit
		//testing  GIT add and commit
		//testing  GIT add and commit ;staging
		
		//testing from beginning
		//testing pull req
		//check newfeaturebranch
		

		//checking merge : m,aster

		//checking : merge : branch
		
		//test :"session1 :add

	}

	// implement here constants used by this extension
}

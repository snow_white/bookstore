/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 8, 2016 3:39:33 PM                      ---
 * ----------------------------------------------------------------
 */
package my.bookstore.facades.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedBookstoreFacadesConstants
{
	public static final String EXTENSIONNAME = "bookstorefacades";
	
	protected GeneratedBookstoreFacadesConstants()
	{
		// private constructor
	}
	
	
}
